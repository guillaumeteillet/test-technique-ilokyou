<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ilokyoutest_model extends CI_Model
{
    public function get_category()
    {
            $query = $this->db->select('category_id, name')
                ->from('categories')
                ->get()
                ->result();

        return $query;
    }

    public function get_items($category, $start, $alpha)
    {
        if ($alpha == 0)
            $alphabetic = "DESC";
        else
            $alphabetic = "ASC";

        if ($category == '0') {
            $query = $this->db->select('object_id, name, category_id, created_at')
                ->from('objects')
                ->limit(10, $start)
                ->order_by('name', $alphabetic)
                ->get()
                ->result();
        }
        else
        {
            $query = $this->db->select('object_id, name, category_id, created_at')
                ->from('objects')
                ->limit(10, $start)
                ->where('category_id', (int) $category)
                ->order_by('name', $alphabetic)
                ->get()
                ->result();
        }

        return $query;
    }

    public function count_items($category)
    {
        if ($category == '0') {
            $query = $this->db->select('count(*) AS nb')
                ->from('objects')
                ->get()
                ->result();
        }
        else
        {
            $query = $this->db->select('count(*) AS nb')
                ->from('objects')
                ->where('category_id', (int) $category)
                ->get()
                ->result();
        }

        return $query[0];
    }
}