<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 * Date: 17/07/15
 * Time: 22:01
 */

class ILokYouTest extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('assets');
        $this->load->model('ilokyoutest_model', 'ilokyou');
        $this->load->library("pagination");
    }

    public function index($category = "", $page = 1, $alpha = 1)
    {
        $data = array();

        $data['category'] = $this->ilokyou->get_category();

        $i = 0;
        while (sizeof($data['category']) > $i)
        {
            $data['catName'][$data['category'][$i]->category_id] = $data['category'][$i]->name;
            $i++;
        }

        $config = array();
        $config["base_url"] = base_url() . "ilokyoutest/index/".$category;
        $config["total_rows"] = $this->ilokyou->count_items($category)->nb;
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;
        $data['items'] = $this->ilokyou->get_items($category, $page, $alpha);
        $data["links"] = $this->pagination->create_links();
        $data["page"] = $page;
        $data['idcat'] = $category;
        $data['orderAlpha'] = 1;


        $this->load->view('ILokYouTest', $data);

    }
}