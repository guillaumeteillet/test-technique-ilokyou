<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Bienvenue sur mon Test ILokYou</title>
</head>
<body>

<div id="container">
    <?php
    echo img('logo.png', '200px');
    ?>

    <p>
        Vous avez comme objectif principal de développer dans un environnement PHP modèle vue contrôleur (de préférence codeigniter mais sans obligation) un site / une page, qui permet d'afficher une liste d'objets (nom de l'objet + nom de la catégorie + date de création de l'objet), et de pouvoir trier ces objets par ordre alphabétique ascendant / descendant ou date de création ascendant / descendant et de les filtrer par nom ou nom de catégorie.
        Vous trouverez ci-joint les tables des objets et des catégories desquelles vous avez besoin.</p>
    <p>Si possible, vous pouvez rajouter une pagination (10 résultats par page par exemple), puis la gérer en ajax (rechargement de la liste des résultats uniquement, et non de la page entière), rendre la page responsive, filtrer / trier par date avec un datepicker jQuery, ou tout ce qui vous passe par la tête comme rajout / amélioration.</p>


	<div id="body">
        Les Catégories :<br/>
        <a href="<?php echo site_url('ilokyoutest'); ?>/index/0/1">Tous</a><br/>
        <?php
        $i = 0;
        while (sizeof($category) > $i)
        {
            echo "<a href='".site_url('ilokyoutest')."/index/".$category[$i]->category_id."/1'>".$category[$i]->name."</a><br/>";
            $i++;
        }
        ?>
	</div>
</div>

</body>
</html>